const fs = require('fs');
const express = require('express');
const router = express.Router();

const { saveName } = require("../services/user.service");
const { isAuthorized } = require("../middlewares/auth.middleware");

//получение массива всех пользователей
router.get('/', (req, res, next) => {
  let allUsers = fs.readFileSync("userlist.json");
  if (allUsers) {
    res.send(allUsers);
  }
  res.status(404).send("The users weren't found!");
});

//получение одного пользователя по ID
router.get('/:id', (req, res, next) => {
  let allUsers = fs.readFileSync("userlist.json");
  allUsers = JSON.parse(allUsers);
  allUsers.forEach((user) => {
    if (user._id == req.params.id) {
      res.send(user);
    }
  });
  res.status(404).send("The user wasn't found!");
});

//создание пользователя по данным передаваемым в теле запроса
router.post('/', (req, res, next) => {
  let newUser = req.body;
  if (newUser) {
    let allUsers = fs.readFileSync("userlist.json");
    allUsers = JSON.parse(allUsers);
    for (let curr = 0; curr < allUsers.length; curr++) {
      if (newUser._id == allUsers[curr]._id) {
        res.status(400).send("User with this ID already exists!");
        return;
      }
    }
    allUsers.push(newUser);
    fs.writeFileSync("userlist.json", JSON.stringify(allUsers));
    res.send('The user was created and added to a list of other users.');
  }
  res.status(400).send("The user wasn't created!");
});

//обновление пользователя по данным передаваемым в теле запроса
router.put('/:id', (req, res, next) => {
  const data = req.body;

  if (data._id) {
    res.status(400).send("You can't update the user ID!");
    return;
  }

  if (isFinite(req.params.id) && data) {
    let allUsers = fs.readFileSync("userlist.json");
    allUsers = JSON.parse(allUsers);
    allUsers.forEach((user) => {
      if (user._id == req.params.id) {
        for (let key in user) {
          user[key] = data[key] || user[key];
        }
        fs.writeFileSync("userlist.json", JSON.stringify(allUsers));
        res.send('The user was updated.');
      }
    });
  }
  res.status(404).send("The user doesn't exist!");
});

//удаление одного пользователя по ID
router.delete('/:id', (req, res, next) => {
  if (isFinite(req.params.id)) {
    let allUsers = fs.readFileSync("userlist.json");
    allUsers = JSON.parse(allUsers);
    allUsers.forEach((user, curr) => {
      if (user._id == req.params.id) {
        allUsers.splice(curr, 1);
        fs.writeFileSync("userlist.json", JSON.stringify(allUsers));
        res.send('The user was deleted.');
      }
    });
  }
  res.status(404).send("The user doesn't exist!");
});

module.exports = router;