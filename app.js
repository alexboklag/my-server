const express = require('express');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

const { allowCrossDomain } = require("./middlewares/allowCrossDomain.middleware");
const indexRouter = require('./routes/index');
const userRouter = require('./routes/user');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(allowCrossDomain);

app.use('/', indexRouter);
app.use('/user', userRouter);

module.exports = app;