# My first server on Node.js

## How to start the code

1. `git clone https://alexboklag@bitbucket.org/alexboklag/my-server.git`
2. `cd my-server`
3. `npm i`
4. `npm start`
5. By default server running on [localhost:3000](http://localhost:3000)

## Main task

Write a web server using **Node.js** + **Express.js** that can handle the list of following requests:

- **GET**: _/user_  
  get an array of all users

- **GET**: _/user/:id_  
  get one user by ID

- **POST**: _/user_  
  create user according to the data from the request body

- **PUT**: _/user/:id_  
  update user according to the data from the request body

- **DELETE**: _/user/:id_  
  delete one user by ID